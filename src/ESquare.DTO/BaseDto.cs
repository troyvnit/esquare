﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESquare.DTO
{
    /// <summary>
    /// Base class for all DTOs
    /// </summary>
    public abstract class BaseDto
    {
        public int Id { get; set; }

        public string RowVersion { get; set; }
    }
}
