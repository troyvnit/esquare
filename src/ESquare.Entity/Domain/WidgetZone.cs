﻿using System.Collections.Generic;
using ESquare.Common.Enums;

namespace ESquare.Entity.Domain
{
    public class WidgetZone : BaseAggregateRoot
    {
        public virtual string ZoneName { get; set; }

        public virtual WidgetPosition Position { get; set; }
    }
}
