﻿using System.Collections.Generic;

namespace ESquare.Entity.Domain
{
    public class ItemGroup : BaseAggregateRoot
    {
        public virtual int Name { get; set; }

        public virtual int ParentId { get; set; }

        public virtual ItemGroup Parent { get; set; }

        public virtual ICollection<ItemGroup> Children { get; set; }
    }
}
