﻿using ESquare.Common.Enums;

namespace ESquare.Entity.Domain
{
    public class Widget : BaseAggregateRoot
    {
        public virtual string Name { get; set; }

        public virtual WidgetPosition Position { get; set; }

        public virtual string DataSource { get; set; }

        public virtual string DataTemplate { get; set; }

        public virtual int ZoneId { get; set; }

        public virtual WidgetZone Zone { get; set; }
    }
}
