﻿using ESquare.Common.Enums;

namespace ESquare.Entity.Domain
{
    public class ItemProperty : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual ItemPropertyType Type { get; set; }

        public virtual string Value { get; set; }

        public virtual string Style { get; set; }
    }
}
