﻿using System.Collections.Generic;

namespace ESquare.Entity.Domain
{
    public class Page : BaseAggregateRoot
    {
        public virtual string Name { get; set; }

        public virtual int PackageId { get; set; }

        public virtual Package Package { get; set; }

        public virtual ICollection<WidgetZone> WidgetZones { get; set; }
    }
}
