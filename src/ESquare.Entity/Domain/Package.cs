﻿using System.Collections.Generic;

namespace ESquare.Entity.Domain
{
    public class Package : BaseAggregateRoot
    {
        public virtual PackageType Type { get; set; }
    }
}
