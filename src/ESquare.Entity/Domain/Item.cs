﻿using System.Collections.Generic;

namespace ESquare.Entity.Domain
{
    public class Item : BaseAggregateRoot
    {
        public virtual string Name { get; set; }

        public virtual string Slug { get; set; }

        public virtual int GroupId { get; set; }

        public virtual ItemGroup Group { get; set; }

        public virtual ICollection<ItemProperty> Properties { get; set; } 
    }
}
