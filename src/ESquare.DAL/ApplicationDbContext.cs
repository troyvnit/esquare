﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ESquare.Entity;
using ESquare.Entity.Domain;
using ESquare.Entity.Identity;

namespace ESquare.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        #region Constructor

        static ApplicationDbContext()
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public ApplicationDbContext() : base("name=DefaultConnection")
        {
            this.Configuration.ProxyCreationEnabled = true;
            this.Configuration.LazyLoadingEnabled = true;
        }

        #endregion Constructor

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Entity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Entity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public new DbSet<T> Set<T>() where T : BaseAggregateRoot
        {
            return base.Set<T>();
        }

        /// <summary>
        /// For ASP.NET Identity
        /// </summary>
        /// <returns></returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
