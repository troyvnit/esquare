﻿namespace ESquare.Common.Enums
{
    public enum ItemPropertyType
    {
        String, Number, Currency, Action, Link
    }
}
