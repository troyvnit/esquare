﻿namespace ESquare.Common.Enums
{
    public enum WidgetPosition
    {
        Top, Left, Bottom, Right
    }
}
