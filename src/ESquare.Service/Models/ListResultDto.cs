﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESquare.DTO;

namespace ESquare.Service.Models
{
    public class ListResultDto<TDto> where TDto : BaseDto
    {
        public IEnumerable<TDto> Items { get; set; }
        public int ItemsCount { get; set; }
    }
}
