﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace ESquare.Service.Mappers.Contracts
{
    public interface ICreateMapping
    {
        /// <summary>
        /// Defines the mapping and adds it into the mapping profile
        /// </summary>
        /// <param name="profile"></param>
        void CreateMapping(Profile profile);
    }
}
