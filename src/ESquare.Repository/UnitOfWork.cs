﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ESquare.DAL;
using ESquare.Entity;
using ESquare.Entity.Domain;
using ESquare.Repository.Constants;

namespace ESquare.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext _dbContext;

        private bool _disposed;
        private Hashtable _repositories;
        private DbContextTransaction _transaction;

        public UnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<T> ExecuteSqlQuery<T>(string sqlQuery, params object[] parameters)
        {
            return _dbContext.Database.SqlQuery<T>(sqlQuery, parameters);
        }

        public void ExecuteSqlCommand(string sqlCommand, params object[] parameters)
        {
            _dbContext.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        //public DateTime GetDatabaseServerTime()
        //{
        //    var objectContext = ((IObjectContextAdapter)this).ObjectContext;
        //    var serverDateTime = objectContext.CreateQuery<DateTime>("CurrentDateTime() ").AsEnumerable().FirstOrDefault();
        //    return serverDateTime;
        //}

        public void SaveChanges(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None)
        {
            bool saveFailed;

            switch (strategy)
            {
                case ConcurrencyResolutionStrategy.None:
                    _dbContext.SaveChanges();
                    break;
                case ConcurrencyResolutionStrategy.DatabaseWin:
                    do
                    {
                        saveFailed = false;

                        try
                        {
                            _dbContext.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update the values of the Entity that failed to save from the store 
                            ex.Entries.Single().Reload();
                        }

                    } while (saveFailed);

                    break;
                case ConcurrencyResolutionStrategy.ClientWin:
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            _dbContext.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update original values from the database 
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }

                    } while (saveFailed);


                    break;
                default:
                    _dbContext.SaveChanges();
                    break;
            }
        }

        public async Task SaveChangesAsync(ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None)
        {
            bool saveFailed;

            switch (strategy)
            {
                case ConcurrencyResolutionStrategy.None:
                    await _dbContext.SaveChangesAsync();
                    break;
                case ConcurrencyResolutionStrategy.DatabaseWin:
                    do
                    {
                        saveFailed = false;

                        try
                        {
                            await _dbContext.SaveChangesAsync();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update the values of the Entity that failed to save from the store 
                            ex.Entries.Single().Reload();
                        }

                    } while (saveFailed);

                    break;
                case ConcurrencyResolutionStrategy.ClientWin:
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            await _dbContext.SaveChangesAsync();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update original values from the database 
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }

                    } while (saveFailed);


                    break;
                default:
                    await _dbContext.SaveChangesAsync();
                    break;
            }
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken, ConcurrencyResolutionStrategy strategy = ConcurrencyResolutionStrategy.None)
        {
            bool saveFailed;

            switch (strategy)
            {
                case ConcurrencyResolutionStrategy.None:
                    await _dbContext.SaveChangesAsync(cancellationToken);
                    break;
                case ConcurrencyResolutionStrategy.DatabaseWin:
                    do
                    {
                        saveFailed = false;

                        try
                        {
                            await _dbContext.SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update the values of the Entity that failed to save from the store 
                            ex.Entries.Single().Reload();
                        }

                    } while (saveFailed);

                    break;
                case ConcurrencyResolutionStrategy.ClientWin:
                    do
                    {
                        saveFailed = false;
                        try
                        {
                            await _dbContext.SaveChangesAsync(cancellationToken);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            saveFailed = true;

                            // Update original values from the database 
                            var entry = ex.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }

                    } while (saveFailed);


                    break;
                default:
                    await _dbContext.SaveChangesAsync(cancellationToken);
                    break;
            }
        }

        public T Reload<T>(T item) where T : BaseAggregateRoot
        {
            _dbContext.Entry(item).Reload();
            return item;
        }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            _transaction = _dbContext.Database.BeginTransaction(isolationLevel);
        }

        public bool CommitTransaction()
        {
            _transaction.Commit();
            _transaction.Dispose();
            return true;
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        public IRepository<T> Repository<T>() where T : BaseAggregateRoot
        {
            if (_repositories == null)
                _repositories = new Hashtable();

            var type = typeof(T).Name;

            if (!_repositories.ContainsKey(type))
            {
                var repositoryType = typeof(Repository<>);

                var repositoryInstance =
                    Activator.CreateInstance(repositoryType
                            .MakeGenericType(typeof(T)), _dbContext);

                _repositories.Add(type, repositoryInstance);
            }

            return (IRepository<T>)_repositories[type];
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
