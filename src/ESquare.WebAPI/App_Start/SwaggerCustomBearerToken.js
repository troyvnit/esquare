﻿(function () {
    $(function () {
        var basicAuthUI =
            '<div class="input"><input placeholder="bearer token" id="input_bearer" name="bearertoken" type="text" size="20"></div>'
        $(basicAuthUI).insertBefore('#api_selector div.input:last-child');
        $("#input_apiKey").hide();
        $('#input_bearer').change(addAuthorization);

        function addAuthorization() {
            var key = $('#input_bearer')[0].value;
            if (key && key.trim() != "") {
                key = "Bearer " + key;
                swaggerUi.api.clientAuthorizations.add("key", new SwaggerClient.ApiKeyAuthorization("Authorization", key, "header"));
            }
        }
    });
})();


